import React from "react";
import ReactDOM from "react-dom";

//Styles
import "./scss/app.scss";

//App
import App from "./App";

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById("root")
);
