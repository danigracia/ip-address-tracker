import L from "leaflet";
import { Marker } from "react-leaflet";
import iconLocation from "../img/icon-location.svg";

//Icon
const locationIcon = () => {
    return L.icon({
        iconUrl: iconLocation,
        iconSize: [46, 56],
    });
};

const MapMarker = ({ latLng }) => {
    return <Marker position={latLng} icon={locationIcon()} />;
};

export default MapMarker;
