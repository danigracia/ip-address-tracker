import { useEffect } from "react";
import MapMarker from "./MapMarker";

//Leaflet
import { MapContainer, TileLayer, useMap } from "react-leaflet";
import "leaflet/dist/leaflet.css";

function PanTo({ latLng }) {
    const map = useMap();
    map.panTo(latLng);
    return null;
}

const Map = ({ latLng }) => {
    return (
        <MapContainer center={latLng} zoom={8} className="map">
            <PanTo latLng={latLng} />
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="© <a href='https://osm.org/copyright'>OpenStreetMap</a> contributors"
            />
            <MapMarker latLng={latLng} />
        </MapContainer>
    );
};

export default Map;
