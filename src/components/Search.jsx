import IconArrow from "../img/icon-arrow.svg";

const Search = ({ address, setAddress, handleSubmit }) => {
    return (
        <form className="search" onSubmit={handleSubmit}>
            <input
                type="text"
                name="address"
                placeholder="Search for any IP address or domain"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
            />
            <button type="submit">
                <img src={IconArrow} alt="Arrow" />
            </button>
        </form>
    );
};

export default Search;
