const Data = ({ data }) => {
    const { address, location, isp, timezone } = data;

    return (
        <div className="data">
            {/* Address */}
            <section className="data-box">
                <h2>Ip address</h2>
                <p>{address}</p>
            </section>

            {/* Location */}
            <section className="data-box">
                <h2>Location</h2>
                <p>{location}</p>
            </section>

            {/* Timezone */}
            <section className="data-box">
                <h2>Timezone</h2>
                <p>{timezone}</p>
            </section>

            {/* ISP */}
            <section className="data-box">
                <h2>Isp</h2>
                <p>{isp}</p>
            </section>
        </div>
    );
};

export default Data;
