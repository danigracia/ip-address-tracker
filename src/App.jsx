import { useState } from "react";
import useSearch from "./hooks/useSearch";
import Search from "./components/Search";
import Data from "./components/Data";
import Map from "./components/Map";

function App() {
    const [address, setAddress] = useState("");
    const [data, setData] = useState({
        latLng: [40.821, -73.847],
        address: "192.212.174.101",
        location: "Brooklyn, NY 10001",
        timezone: "UTC -05:00",
        isp: "SpaceX Starlink",
    });

    const { searchAddress } = useSearch();

    const handleSubmit = async (e) => {
        e.preventDefault();

        //Get address data
        setData(await searchAddress(address));
    };

    return (
        <>
            <main className="main">
                <h1 className="title">IP Address Tracker</h1>
                <Search
                    address={address}
                    setAddress={setAddress}
                    handleSubmit={handleSubmit}
                />

                <Data data={data} />
            </main>
            <Map latLng={data.latLng} />
        </>
    );
}

export default App;
