import axios from "axios";

const useSearch = () => {
    //Search address data
    const searchAddress = async (address) => {
        const url = `https://geo.ipify.org/api/v2/country,city?apiKey=at_dBbRSrA9bXBhK6UVcscBrqzSo3gEl&ipAddress=${address}`;

        const response = await axios.get(url);

        //Get data
        const latLng = [
            Number(response.data.location.lat),
            Number(response.data.location.lng),
        ];
        const location = `${response.data.location.city}, ${response.data.location.country} ${response.data.location.postalCode}`;
        const isp = response.data.isp;
        const timezone = `UTC ${response.data.location.timezone}`;

        return {
            latLng,
            address,
            location,
            isp,
            timezone,
        };
    };

    return {
        searchAddress,
    };
};

export default useSearch;
